<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 20.02.15
 * Time: 17:01
 */

namespace CMS\GalleryBundle\Twig;


use CMS\GalleryBundle\Services\ImagesService;
use CMS\GalleryBundle\Tools\Image;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GalleryExtension extends \Twig_Extension
{
    private $imagesService;

    private $optionResolver;

    public function __construct(ImagesService $imagesService)
    {
        $this->imagesService = $imagesService;
        $resolver = new OptionsResolver();

        $resolver
            ->setDefined(['type', 'width', 'height', 'size', 'isFullPath'])
            ->setRequired('type')
            ->setDefaults([
                'type' => 'proportions',
                'isFullPath' => false
            ])
            ->setAllowedTypes('isFullPath', 'bool')
            ->setAllowedValues('type', [
                Image::IMAGE_RESIZE_TYPE_PROPORTIONS,
                Image::IMAGE_RESIZE_TYPE_BY_WIDTH,
                Image::IMAGE_RESIZE_TYPE_BY_HEIGHT,
                Image::IMAGE_RESIZE_TYPE_BY_MIN_SIDE
            ]);

        $this->optionResolver = $resolver;
    }

    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('image_url', [$this, 'imageUrl'], ['is_safe' => ['text']])
        );
    }

    public function imageUrl($path, array $options)
    {
        if(empty($path)){
            return;
        }

        $options = $this->optionResolver->resolve($options);

        switch ($options['type']) {
            case Image::IMAGE_RESIZE_TYPE_PROPORTIONS:
                $this->optionResolver
                    ->setRequired('width')
                    ->setRequired('height');

                $options = $this->optionResolver->resolve($options);

                return $this->imagesService->resizeImageOfSize(
                    $path,
                    $options['width'] . 'x' . $options['height'],
                    $options['isFullPath']
                );
            case Image::IMAGE_RESIZE_TYPE_BY_WIDTH:
                $this->optionResolver
                    ->setRequired('width');

                $options = $this->optionResolver->resolve($options);

                return $this->imagesService->resizeImageByWidth(
                    $path,
                    $options['width'],
                    $options['isFullPath']
                );
            case Image::IMAGE_RESIZE_TYPE_BY_HEIGHT:
                $this->optionResolver
                    ->setRequired('height');

                $options = $this->optionResolver->resolve($options);

                return $this->imagesService->resizeImageByHeight(
                    $path,
                    $options['height'],
                    $options['isFullPath']
                );
            case Image::IMAGE_RESIZE_TYPE_BY_MIN_SIDE:
                $this->optionResolver
                    ->setRequired('size');

                $options = $this->optionResolver->resolve($options);

                return $this->imagesService->resizeImageByMinSide(
                    $path,
                    $options['size'],
                    $options['isFullPath']
                );
            default:
                return null;
        }
    }

    public function getName()
    {
        return 'cms_gallery_extension';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.02.15
 * Time: 12:54
 */
namespace CMS\GalleryBundle\Services;

use CMS\CoreBundle\AbstractCoreService;
use CMS\GalleryBundle\Form\ImagesType;
use CMS\GalleryBundle\Tools\Image;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImagesService extends AbstractCoreService
{
    private $allowedImagesExtensions = [];

    private $maxFileSize;

    private $uploadImagesDirectory;


    /**
     * @var array
     */
    public $allowedImageSize = array(
        '32x32' => array(32, 32),
        '64x64' => array(64, 64),
        '128x128' => array(128, 128),
        '256x256' => array(256, 256),
        '512x512' => array(512, 512)
    );


    /**
     * @param UploadedFile $image
     * @return string
     */
    public function uploadImage(UploadedFile $image)
    {
        $this->validate($image);

        $filename = sha1(uniqid(mt_rand(), true));
        $path = $filename . '.' . $image->guessExtension();

        try {
            $image->move($this->getUploadImageRootDir(), $path);
        } catch (FileException $e) {
            throw new \CMS\GalleryBundle\Exception\FileException($e->getMessage());
        }

        return $path;
    }

    /**
     * @param $path
     * @param $size
     * @param bool $isFullPath
     * @return bool|null|string
     */
    public function resizeImageOfSize($path, $size, bool $isFullPath = false)
    {
        if (!array_key_exists($size, $this->allowedImageSize)) {
            return null;
        }

        list($width, $height) = $this->allowedImageSize[$size];

        $newPath = $this->getUploadImageDirectory() . '/' . $size . '/' . $path;

        if (file_exists($newPath)) {
            return $newPath;
        }

        if(!$isFullPath){
            $path = $this->getUploadImageRootDir() . '/' . $path;
        }

        $image = new Image($path);

        return $image->resize($width, $height, $newPath);
    }


    /**
     * @param string $path
     * @param integer $width
     * @param bool $isFullPath
     * @return mixed
     */
    public function resizeImageByWidth($path, $width, bool $isFullPath = false)
    {
        if(!$isFullPath){
            $path = $this->getUploadImageRootDir() . '/' . $path;
        }

        try {
            $image = new Image($path);
        }
        catch(\CMS\GalleryBundle\Exception\FileException $e){
            return null;
        }

        $height = round($image->getHeight() / ($image->getWidth() / $width));

        $newPath = $this->getUploadImageDirectory() . '/' . $width . 'x' . $height . '/' . basename($path);

        if (file_exists($newPath)) {
            return $newPath;
        }

        return $image->resize($width, $height, $newPath, false);
    }

    /**
     * @param string $path
     * @param integer $height
     * @param bool $isFullPath
     * @return mixed
     */
    public function resizeImageByHeight($path, $height, bool $isFullPath = false)
    {
        if(!$isFullPath){
            $path = $this->getUploadImageRootDir() . '/' . $path;
        }

        try {
            $image = new Image($path);
        }
        catch(\CMS\GalleryBundle\Exception\FileException $e){
            return null;
        }

        $width = round($image->getWidth() / ($image->getHeight() / $height));

        $newPath = $this->getUploadImageDirectory() . '/' . $width . 'x' . $height . '/' . basename($path);

        if (file_exists($newPath)) {
            return $newPath;
        }

        return $image->resize($width, $height, $newPath, false);
    }

    /**
     * @param string $path
     * @param integer $size
     * @param bool $isFullPath
     * @return mixed
     */
    public function resizeImageByMinSide($path, $size, bool $isFullPath = false)
    {
        if(!$isFullPath){
            $path = $this->getUploadImageRootDir() . '/' . $path;
        }

        try {
            $image = new Image($path);
        }
        catch(\CMS\GalleryBundle\Exception\FileException $e){
            return null;
        }

        $widthIsLess  = $image->getWidth() < $size;
        $heightIsLess = $image->getHeight() < $size;

        $resizeBy = null;

        if($widthIsLess && $heightIsLess){
            $resizeBy = $widthIsLess > $heightIsLess ? Image::IMAGE_HEIGHT : Image::IMAGE_WIDTH;
        }
        else if($widthIsLess){
            $resizeBy = Image::IMAGE_WIDTH;
        }
        else{
            $resizeBy = Image::IMAGE_HEIGHT;
        }

        if ($image->getHeight() > $image->getWidth()) {
            return $this->resizeImageByWidth($path, $size);
        }

        return $this->resizeImageByHeight($path, $size, true);
    }

    public function clearResizeImages($path){

        $directoryPath = $this->getUploadImageDirectory();

        if(is_dir($directoryPath) && ($dh = opendir($directoryPath))){
            while(($dir = readdir($dh)) !== false){
                if( $dir == '.' || $dir == '..') continue;

                if(file_exists($directoryPath . '/' . $dir . '/' . $path)){
                    @unlink($directoryPath . '/' . $dir . '/' . $path);
                }
            }

            closedir($dh);
        }

        return true;
    }

    public function getImageSize($path){
        try {
            $image = new Image($this->getUploadImageRootDir() . '/' . $path);
        }
        catch(\CMS\GalleryBundle\Exception\FileException $e){
            return [];
        }

        return [$image->getWidth(), $image->getHeight()];
    }

    private function validate(UploadedFile $file)
    {
        if (!in_array($file->guessClientExtension(), $this->allowedImagesExtensions)) {
            throw new \CMS\GalleryBundle\Exception\FileException(
                sprintf('File extension \'%s\' not allowed!', $file->guessClientExtension())
            );
        }

        if ($file->getSize() > $this->maxFileSize) {
            throw new \CMS\GalleryBundle\Exception\FileException(
                sprintf('File size more than allowed! Allowed %s b', $this->maxFileSize)
            );
        }
    }

    /**
     * @return array
     */
    public function getDefaultsCriteria()
    {
        return array();
    }

    /**
     * Return form for entity
     *
     * @param FormBuilder|FormFactory $form
     * @param null $data
     * @return mixed
     */
    public function configureForm(FormFactory $form, $data = null)
    {
        return $form->createBuilder(
            new ImagesType(),
            $data,
            array(
                'data_class' => $this->getRepositoryClass()
            )
        );
    }

    /**
     * Return name repository for crud
     *
     * @return string
     */
    public function getRepositoryName()
    {
        return 'GalleryBundle:Images';
    }

    /**
     * @return string
     */
    public function getUploadImageRootDir()
    {
        return __DIR__ . '/../../../../../../web/' . $this->getUploadImageDirectory();
    }

    /**
     * @return mixed
     */
    public function getUploadImageDirectory()
    {
        return $this->uploadImagesDirectory;
    }

    /**
     * @param $uploadImagesDirectory
     * @return $this
     */
    public function setUploadImagesDirectory($uploadImagesDirectory)
    {
        $this->uploadImagesDirectory = $uploadImagesDirectory;

        return $this;
    }

    /**
     * @return array
     */
    public function getAllowedImagesExtensions()
    {
        return $this->allowedImagesExtensions;
    }

    /**
     * @param array $allowedImagesExtensions
     */
    public function setAllowedImagesExtensions($allowedImagesExtensions)
    {
        $this->allowedImagesExtensions = $allowedImagesExtensions;
    }

    /**
     * @return mixed
     */
    public function getMaxFileSize()
    {
        return $this->maxFileSize;
    }

    /**
     * @param mixed $maxFileSize
     */
    public function setMaxFileSize($maxFileSize)
    {
        $this->maxFileSize = $maxFileSize;
    }
}
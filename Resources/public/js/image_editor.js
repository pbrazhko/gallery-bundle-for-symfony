/**
 * Created by pavel on 25.12.15.
 */
(function ($) {
    var defaultConfig = {
        'images_directory': null,
        'rotate_left_icon': null,
        'rotate_right_icon': null,
        'crop_icon': null,
        'route_resize': null,
        'route_crop': null,
        'route_rotate': null,
        'route_info': null
    };

    var imageEditor = {
        config: {},
        image: null,
        imageId: null,
        originImage: null,
        editorContainer: null,
        editorImage: null,
        zoom: 1,
        jcrop: null,
        init: function (id, config) {
            this.imageId = id;

            this.config = $.extend({}, defaultConfig, config);

            if (this.config.images_directory == null) {
                throw new Error('Images directory is not defined!');
            }

            this.loadImage(id);
        },
        initModal: function () {
            this.editorContainer = $('<div/>');
            var windowSize = $.modal.impl.getWindowSize();
            var contentWidth = Math.round(windowSize[0] * 0.70);
            var contentHeight = Math.round(windowSize[1] * 0.70);

            this.editorImage = $('<img/>').attr(this.buildImageAttr(contentWidth, contentHeight));

            this.editorContainer
                .attr('style', 'width:' + contentWidth + 'px;height:' + contentHeight + 'px;overflow-y:hidden;position:relative;background-color:#ccc;text-align:center;')
                .append(this.editorImage);

            this.editorContainer.append(this.buildToolbar(contentWidth, contentHeight));

            $.modal({
                'title': 'Edit image',
                'showFooter': false,
                'content': this.editorContainer
            });
        },
        loadImage: function (id) {
            if (!this.config.route_info || this.config.route_info == '') {
                throw new Error('Route load info of image is not defined!');
            }

            var self = this;
            var data = {'id': id};
            var callback = function (result) {
                self.image = result;
                self.initModal();
            };

            this.send(
                Routing.generate(this.config.route_info, {'id': id}),
                'POST',
                data,
                callback
            );
        },
        rotateImage: function(degree){
            if (!this.config.route_rotate || this.config.route_rotate == '') {
                throw new Error('Route rotate of image is not defined!');
            }

            var self = this;
            var data = {'id': this.image.id, 'degree': degree};
            var callback = function (result) {
                self.image = result;

                self.reloadImage(self.imageId);
            };

            this.send(
                Routing.generate(this.config.route_rotate, {'id': this.image.id}),
                'POST',
                data,
                callback
            );
        },
        crop: function () {
            if (!this.config.route_crop || this.config.route_crop == '') {
                throw new Error('Crop rotate of image is not defined!');
            }

            var coords = this.jcrop.tellSelect();

            var self = this;
            var data = {
                'id': this.image.id,
                'x': Math.floor(coords.x*this.zoom),
                'y': Math.floor(coords.y*this.zoom),
                'width': Math.floor(coords.w*this.zoom),
                'height': Math.floor(coords.h*this.zoom)
            };

            var callback = function (result) {
                self.image = result;

                self.jcrop.destroy();

                self.reloadImage(self.imageId);
            };

            this.send(
                Routing.generate(this.config.route_crop, {'id': this.image.id}),
                'POST',
                data,
                callback
            );
        },
        buildToolbar: function(contentWidth, contentHeight){
            var self = this;
            var toolbarWidth = Math.round(contentWidth * 0.30);
            var toolbarHeight = Math.round(contentHeight * 0.04);
            var toolbarLeft = Math.round(Math.round(contentWidth/2) - Math.round(toolbarWidth/2));

            var toolbar = $('<div/>')
                .attr('style', 'position:absolute;width:' + toolbarWidth + 'px;height:' + toolbarHeight + 'px;background-color:#000000;opacity: 0.7;bottom:0px;left:' + toolbarLeft + 'px;text-align:center;z-index:610;padding:5px;');

            if(this.config.route_rotate){
                var buttonRotateLeftLabelText = 'Rotate left';
                var buttonRotateLeftLabel = this.config.rotate_left_icon ? $('<img/>').attr({'src':this.config.rotate_left_icon, 'style': 'height:100%'}) : '<b style="color:#ffffff">' + buttonRotateLeftLabelText + '</b>';
                var buttonRotateLeft = $('<button/>')
                    .attr({
                        'style': 'background:none;border:none;vertical-align:middle;cursor:pointer;height:100%;',
                        'title': buttonRotateLeftLabelText
                    })
                    .html(buttonRotateLeftLabel);

                buttonRotateLeft.on('click', function(){
                    self.rotateImage(90);
                });

                toolbar.append(buttonRotateLeft);

                var buttonRotateRightLabelText = 'Rotate right';
                var buttonRotateRightLabel = this.config.rotate_right_icon ? $('<img/>').attr({'src':this.config.rotate_right_icon, 'style': 'height:100%'}) : '<b style="color:#ffffff">' + buttonRotateRightLabelText + '</b>';
                var buttonRotateRight = $('<button/>')
                    .attr({
                        'style': 'background:none;border:none;vertical-align:middle;cursor:pointer;height:100%;',
                        'title': buttonRotateRightLabelText
                    })
                    .html(buttonRotateRightLabel);

                buttonRotateRight.on('click', function(){
                    self.rotateImage(270);
                });

                toolbar.append(buttonRotateRight);
            }

            if(this.config.route_crop && $.isFunction($.fn.Jcrop)){
                var buttonCropLabelText = 'Crop';
                var buttonCropLabel = this.config.crop_icon ? $('<img/>').attr({'src':this.config.crop_icon, 'style': 'height:100%'}) : '<b style="color:#ffffff">' + buttonCropLabelText + '</b>';
                var buttonCrop = $('<button/>')
                    .attr({
                        'style': 'background:none;border:none;vertical-align:middle;cursor:pointer;height:100%;',
                        'title': buttonCropLabelText
                    })
                    .html(buttonCropLabel);


                this.editorImage.Jcrop({}, function(){
                    self.jcrop = this;
                });

                buttonCrop.on('click', function(){
                    self.crop();
                });

                toolbar.append(buttonCrop);
            }

            return toolbar;
        },
        buildImageAttr: function (contentWidth, contentHeight) {
            var imageAttr = {
                'data-id': this.imageId,
                'src': '/' + this.config.images_directory + '/' + this.image.path + '?' + new Date().getTime(),
            };

            if (this.image.width > contentWidth || this.image.height > contentHeight) {
                if (Math.abs(contentWidth - this.image.width) > Math.abs(contentHeight - this.image.height)) {
                    imageAttr.width = '100%';
                    this.zoom = this.calculateZoom(contentWidth, this.image.width);
                }
                else {
                    imageAttr.height = '100%';
                    this.zoom = this.calculateZoom(contentHeight, this.image.height);
                }
            }

            return imageAttr;
        },
        calculateZoom: function(containerSize, imageSize){
            return parseFloat(imageSize/containerSize).toFixed(2);
        },
        reloadImage: function(imageId){
            var self = this;
            if (null !== this.jcrop) {
                self.jcrop.destroy();
            }

            var windowSize = $.modal.impl.getWindowSize();
            var contentWidth = Math.round(windowSize[0] * 0.70);
            var contentHeight = Math.round(windowSize[1] * 0.70);

            $('img[data-id="' + imageId + '"]', this.editorContainer).each(function (k, v) {
                var newImage = $('<img/>').attr(self.buildImageAttr(contentWidth, contentHeight));

                $(v).replaceWith(newImage);

                if (null !== self.jcrop) {
                    $(newImage).Jcrop({}, function () {
                        self.jcrop = this;
                    });
                }
            });
        },
        send: function (url, method, data, callback) {
            $.ajax({
                'url': url,
                'method': method,
                'success': callback,
                'data': data
            })
        }
    };

    $.fn.imageEditor = function (config) {
        if ($(this).data('id') == undefined) {
            return;
        }

        return imageEditor.init($(this).data('id'), config);
    }
})(jQuery);
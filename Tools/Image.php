<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.02.15
 * Time: 12:47
 */

namespace CMS\GalleryBundle\Tools;

use CMS\GalleryBundle\Exception\FileException;

class Image
{
    const IMAGE_WIDTH = 'width';
    const IMAGE_HEIGHT = 'height';

    const IMAGE_RESIZE_TYPE_PROPORTIONS = 1;
    const IMAGE_RESIZE_TYPE_BY_WIDTH = 2;
    const IMAGE_RESIZE_TYPE_BY_HEIGHT = 3;
    const IMAGE_RESIZE_TYPE_BY_MIN_SIDE = 4;

    /**
     * @var string
     */
    private $filename;

    /**
     * @var string
     */
    private $extension;

    /**
     * @var integer
     */
    private $width;

    /**
     * @var integer
     */
    private $height;

    /**
     * @var integer
     */
    private $size;

    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var string
     */
    private $image;


    /**
     * @param null $image
     */
    public function __construct($image = null)
    {
        if (null === $image || !file_exists($image)){
            throw new FileException(sprintf('Image %s not found!', $image));
        };

        $this->path = $image;
        $this->size = filesize($this->path);

        list($this->filename, $this->extension) = explode('.', basename($this->path));

        $imageInfo = @getimagesize($this->path);

        if ($imageInfo !== false) {
            $this->width = $imageInfo[0];
            $this->height = $imageInfo[1];
            $this->mimeType = $imageInfo['mime'];
        }

        return true;
    }

    /**
     * @param $newWidth
     * @param $newHeight
     * @param bool $newPath
     * @param bool $proportions
     * @return bool
     */
    public function resize($newWidth, $newHeight, $newPath = false, $proportions = true)
    {
        if (!file_exists($this->path)) return false;

        if ($proportions) {
            if ($this->width > $this->height) {
                $newHeight = $this->height / ($this->width / $newWidth);
            } else {
                $newWidth = $this->width / ($this->height / $newHeight);
            }
        }

        if (false === $newPath) $newPath = $this->path;

        $this->getNewImage($newWidth, $newHeight);
        $sourceImage = $this->getImageFromSource();

        @imagecopyresampled($this->image, $sourceImage, 0, 0, 0, 0, $newWidth, $newHeight, $this->width, $this->height);

        return $this->save($newPath);
    }

    public function rotate($angel, $path = null, $backgroundColor = 0, $saveTransparent = false){
        if (!file_exists($this->path)) return false;

        $this->image = @imagerotate($this->getImageFromSource(), $angel, $backgroundColor, !$saveTransparent);

        if(null === $path){
            $path = $this->path;
        }

        return $this->save($path);
    }

    public function crop($x, $y, $width, $height, $path = null){
        if (!file_exists($this->path)) return false;

        $this->image = @imagecrop($this->getImageFromSource(), [
            'x' => $x,
            'y' => $y,
            'width' => $width,
            'height' => $height
        ]);

        if(null === $path){
            $path = $this->path;
        }

        return $this->save($path);
    }

    /**
     * @param $width
     * @param $height
     * @return $this
     */
    private function getNewImage($width, $height)
    {
        $this->image = imagecreatetruecolor($width, $height);

        return $this;
    }

    /**
     * @return resource
     */
    private function getImageFromSource()
    {
        switch ($this->mimeType) {
            case "image/jpg":
            case "image/jpeg":
                $image = imagecreatefromjpeg($this->path);
                break;
            case 'image/gif':
                $image = imagecreatefromgif($this->path);
                break;
            case 'image/png':
                $image = imagecreatefrompng($this->path);
                break;
        }

        return $image;
    }

    /**
     * @param $newPath
     * @return mixed
     */
    private function save($newPath)
    {
        if ($newPath) {
            $directory = dirname($newPath);

            if (!is_dir($directory)) {
                @mkdir($directory, 0777, true);
            }
        }

        @imagejpeg($this->image, $newPath, 80);

        return $newPath;
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 19.02.15
 * Time: 12:57
 */

namespace CMS\GalleryBundle\Form\Types;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImagesType extends AbstractType
{
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'entry_type' => ImageType::class,
            'allow_add' => true,
            'allow_delete' => true,
            'delete_empty' => true
        ));
    }

    public function getParent()
    {
        return 'Symfony\Component\Form\Extension\Core\Type\CollectionType';
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'cms_gallery_images_type';
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 08.09.14
 * Time: 16:56
 */

namespace CMS\GalleryBundle\Response;

use CMS\GalleryBundle\Normalizer\ImagesNormalizer;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Serializer;

class ImagesJsonResponse extends Response
{
    public function __construct($data, $status = 200, $headers = array())
    {
        $serializer = $this->getSerializer();

        parent::__construct($serializer->serialize($data, 'json'), $status, $headers);
    }

    private function getSerializer()
    {
        return new Serializer(array(
                new ImagesNormalizer()
            ),
            array(
                new JsonEncoder()
            )
        );
    }
} 
/**
 * Created by pavel on 14.03.15.
 */
$(document).ready(function () {
    var imagesPrototype = $('#gallery_images').data('prototype');
    var imagesIndex = $('#gallery_images').find('.thumbnail').length;

    $('#gallery_images').find('.thumbnail').each(function (index, image) {
        var controlPanel = $(image).find('.control-panel');

        controlPanel.find('.delete-img').click(function (event) {
            $(this).closest('.thumbnail').remove();
        });
    });

    $('#imageupload').fileupload({
        dataType: 'json',
        formData: {size: '256x256'},
        send: function () {
            $('#imageupload').attr('disabled', 'disabled');
        },
        done: function (e, data) {
            $.each(data.result, function (index, image) {
                if (image.status !== 'ok') {
                    console.info(image.error);
                }
                else {
                    imagesIndex = imagesIndex + 1;

                    var newImage = $.parseHTML(imagesPrototype.replace(/__name__/g, imagesIndex));

                    var controlPanel = $(newImage).find('.control-panel');

                    controlPanel.find('.delete-img').click(function (event) {
                        $(this).closest('.thumbnail').remove();
                    });

                    $(newImage).find('img').attr('src', '/' + image.path_size);
                    $(newImage).find('input[type="hidden"]').val(image.path);
                    $('#gallery_images').append(newImage);
                }

                $('#imageupload').removeAttr('disabled');
            })
        }
    });
});
<?php

namespace CMS\GalleryBundle\Controller;

use CMS\GalleryBundle\Exception\FileException;
use CMS\GalleryBundle\Response\ImagesJsonResponse;
use CMS\GalleryBundle\Response\ErrorJsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Request;

class ImagesController extends Controller
{
    public function uploadAction(Request $request)
    {
        $service = $this->get('cms.gallery.images.service');

        if ($request->files->count() <= 0) {
            return new ErrorJsonResponse(sprintf('Nothing upload'));
        }

        $result = array();
        /** @var UploadedFile $file */
        foreach ($request->files->getIterator() as $file) {
            $image = array();

            $image = array_merge($image, array('file' => $file->getClientOriginalName()));

            try {
                $path = $service->uploadImage($file);

                $image = array_merge($image, array('status' => 'ok'));
            } catch (FileException $e) {
                $image = array_merge($image, array('status' => 'fail'));
                $image = array_merge($image, array('error' => $e->getMessage()));
                $result[] = $image;

                continue;
            }

            $image = array_merge($image, array('path' => $path));

            if ($request->request->has('size')) {
                $path_size = $service->resizeImageOfSize($path, $request->request->get('size', '128x128'));
                $image = array_merge($image, array('path_size' => $path_size));
            }

            $result[] = $image;
        }

        return new ImagesJsonResponse($result);
    }
}

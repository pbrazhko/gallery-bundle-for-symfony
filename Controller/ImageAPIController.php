<?php
/**
 * Created by PhpStorm.
 * User: pavel
 * Date: 25.12.15
 * Time: 17:28
 */

namespace CMS\GalleryBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class ImageAPIController extends Controller
{
    public function getImageAction($id)
    {
        $galleryImageService = $this->get('cms.gallery.images.service');

        if (null === ($image = $galleryImageService->findOneBy(['id' => $id]))) {
            throw $this->createNotFoundException('Image not found');
        }

        return;
    }
}